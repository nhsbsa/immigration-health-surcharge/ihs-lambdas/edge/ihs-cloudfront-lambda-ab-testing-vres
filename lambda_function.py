import json

def parseRequestCookies(headers):
    parsedCookies = {}
    index = 0
    for bigCookies in headers.get('cookie', []):
        for cookie in bigCookies['value'].split(';'):
            if cookie:
                parsedCookies[index] = cookie.strip()
                index = index + 1
    return parsedCookies
    
def parseResponseCookies(headers):
    parsedCookies = {}
    index = 0
    for bigCookies in headers.get('set-cookie', []):
        for cookie in bigCookies['value'].split(';'):
            if cookie:
                parsedCookies[index] = cookie.strip()
                index = index + 1
    return parsedCookies
    
def lambda_handler(event, context):
    response = event['Records'][0]['cf']['response']
    request = event['Records'][0]['cf']['request']
    res_headers = response['headers']
    req_headers = request['headers']
    
    cookieExperiments = 'X-IHS-Experiment=true'
    cookieExperimentA = 'X-IHS-Experiment-Name=A'
    cookieExperimentB = 'X-IHS-Experiment-Name=B'

    setCookieA = False
    setCookieB = False
    cookieExperient = False
    
    if 'set-cookie' not in response['headers']:
        response['headers']['set-cookie'] = []

    print('VIEWER-RESPONSE')
    parsedRequestCookies = parseRequestCookies(req_headers)
    #parsedResponseCookies = parseResponseCookies(res_headers)
    
    #if parsedResponseCookies:
    #    for index, cookie in parsedResponseCookies.items():
    #        if (cookie == cookieExperimentA) or (cookie == cookieExperimentB):
    #            setCookieA = False
    #            setCookieB = False
    #            noCookiesSet = False
    #            print('Found set-cookie')

    if parsedRequestCookies:
        for index, cookie in parsedRequestCookies.items():
            if cookie == cookieExperiments:
                cookieExperient = True
                print('Found request cookie experiment')
            if cookie == cookieExperimentA:
                setCookieA = True
                print('Found request cookie A')
            if cookie == cookieExperimentB:
                setCookieB = True
                print('Found request cookie B')

    if cookieExperient:
        if setCookieA:
            response['headers']['set-cookie'].append({'key': 'Set-Cookie', 'value': cookieExperimentA})
            print('Creating new set-cookie A')
        elif setCookieB:
            response['headers']['set-cookie'].append({'key': 'Set-Cookie', 'value': cookieExperimentB})
            print('Creating new set-cookie B')
        
    return response
